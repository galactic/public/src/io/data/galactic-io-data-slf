"""Population test module."""

from unittest import TestCase

from galactic.io.data.core import PopulationFactory
from galactic.io.data.slf import SLFDataReader


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, SLFDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
