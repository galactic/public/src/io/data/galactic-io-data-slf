"""
SLF  Data reader.
"""

from collections.abc import Iterator, Mapping
from typing import TextIO, cast

from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory


# pylint: disable=too-few-public-methods
class SLFDataReader:
    """
    ̀``SLF`` Data reader.

    Example
    -------
    >>> from pprint import pprint
    >>> from galactic.io.data.slf import SLFDataReader
    >>> reader = SLFDataReader()
    >>> import io
    >>> data = '''[Lattice]
    ... 2
    ... 3
    ... [Objects]
    ... 1 2
    ... [Attributes]
    ... a b c
    ... [Relation]
    ... 0 1 0
    ... 1 1 0
    ... '''
    >>> individuals = reader.read(io.StringIO(data))
    >>> pprint({key: sorted(list(value)) for key, value in individuals.items()})
    {'1': ['b'], '2': ['a', 'b']}

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a ``SLF`` data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        """
        individuals = {}
        # Read header
        data_file.readline()
        attribute_count = int(data_file.readline())
        individual_count = int(data_file.readline())
        #
        # observation table
        #
        data_file.readline()
        current = data_file.readline().strip("\n \r\t").split(" ")
        observations = [current[index] for index in range(attribute_count)]
        #
        # attributes table
        #
        data_file.readline()
        current = data_file.readline().strip("\n \r\t").split(" ")
        attributes = [current[index] for index in range(individual_count)]
        #
        # relation table
        #
        data_file.readline()
        line = data_file.readline()

        for index in range(attribute_count):
            conversion = set()
            current = line.strip("\n \r\t").split(" ")
            for key in range(individual_count):
                if current[key] == "1":
                    conversion.add(attributes[key])
            individuals[observations[index]] = frozenset(conversion)
            line = data_file.readline()
        return individuals

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".slf"])


def register() -> None:
    """
    Register an instance of a ``SLF`` data reader.
    """
    PopulationFactory.register_reader(SLFDataReader())
