from collections.abc import Iterator, Mapping
from typing import TextIO

class SLFDataReader:
    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]: ...
    @classmethod
    def extensions(cls) -> Iterator[str]: ...

def register() -> None: ...
