"""
SLF Data reader.
"""

from ._main import SLFDataReader, register

__all__ = ("SLFDataReader", "register")
