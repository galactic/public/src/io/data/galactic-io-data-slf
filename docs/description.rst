=================
*SLF* data reader
=================

*galactic-io-data-slf* is a data reader
plugin for **GALACTIC**.

The file extension is ``.slf``. The internal format is composed of 4
sections:

* the first section starts with a header containing the keyword ``Lattice``
  surrounded by square brackets. It is followed by two lines containing the
  number of objects and the number of attributes;
* the second section starts with the keyword ``Objects`` surrounded by square
  brackets. It is followed by lines containing the names of the objects;
* the third section starts with the keyword ``Attributes`` surrounded by square
  brackets. It is followed by lines containing the names of the attributes;
* the fourth section starts with the keyword ``Relation`` surrounded by square
  brackets. It is followed by lines containing the binary relation expressed as
  a matrix composed of 0 and 1.

For example:

.. code-block:: text
    :class: admonition

    [Lattice]
    10
    5
    [Objects]
    0
    1
    2
    3
    4
    5
    6
    7
    8
    9
    [Attributes]
    c
    e
    o
    p
    s
    [Relation]
    1 1 0 0 1
    0 0 1 0 1
    0 1 0 1 0
    0 0 1 1 0
    1 1 0 0 1
    0 0 1 1 0
    1 1 0 0 0
    0 0 1 1 0
    1 1 0 0 0
    1 0 1 0 1

