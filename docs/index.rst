.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. include:: description.rst

**GALACTIC** [#logo]_ [#logoreader]_ stands for
**GA**\ lois
**LA**\ ttices,
**C**\ oncept
**T**\ heory,
**I**\ mplicational systems and
**C**\ losures.

..  toctree::
    :maxdepth: 1
    :caption: Getting started

    install/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Learning

    notebook

..  toctree::
    :maxdepth: 1
    :caption: Release Notes

    release-notes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. [#logo]
        The **GALACTIC** icon has been constructed using icons present in the
        `free star wars icons set <http://sensibleworld.com/news/free-star-wars-icons>`_
        designed by `Sensible World <http://www.iconarchive.com/artist/sensibleworld.html>`_.
        The product or characters depicted in these icons
        are © by Disney / Lucasfilm.

        It's a tribute to the french mathematician
        `Évariste Galois <https://en.wikipedia.org/wiki/Évariste_Galois>`_ who
        died at age 20 in a duel.
        In France, Concept Lattices are also called *Galois Lattices*.

.. [#logoreader]
        The *galactic-io-data-slf* icon has been constructed using the main
        **GALACTIC** icon,
        the
        `Floppy, save, guardar icon <https://www.iconfinder.com/icons/326688/floppy_save_guardar_icon>`_
        designed by
        `Google <https://www.iconfinder.com/iconsets/material-core>`_,
        and the
        `Database, data, storage icon <https://www.iconfinder.com/icons/8541628/database_data_storage_icon>`_
        designed by
        `Font Awesome Search Icons <https://www.iconfinder.com/iconsets/font-awesome-solid-vol-1>`_.
